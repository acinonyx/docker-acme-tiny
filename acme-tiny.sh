#!/bin/sh -e
#
# Let's Encrypt certificate generator
#
# Copyright (C) 2018-2019, 2021-2023 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

OPENSSL_KEY_DIR="/var/lib/acme-tiny"
OPENSSL_CERT_DIR="/var/lib/acme-tiny"
CHALLENGES_DIR="/var/lib/acme-tiny/challenges"
ACCOUNT_KEY_FILE="/var/lib/acme-tiny/account.key"
DAYS="31"

usage() {
	/bin/cat <<EOF
Usage: acme-tiny.sh [OPTIONS]... DOMAINS...
Generate or renew a Let's Encrypt certificate

  -b                        Number of bits of the key
  -r                        Account email
  -n                        Name of certificate
  -c                        Configuration file or directory
  --help                    Print usage

EOF
	exit 1
}

read_config() {
	if [ -f "$1" ]; then
		unset \
			ACME_TINY_NUMBITS \
			ACME_TINY_ACCOUNT_EMAIL \
			ACME_TINY_NAME \
			ACME_TINY_DOMAINS \
			ACME_TINY_SELF_SIGNED
		# shellcheck source=/dev/null
		. "$1"
		numbits="${ACME_TINY_NUMBITS:-4096}"
		account_email="$ACME_TINY_ACCOUNT_EMAIL"
		name="$ACME_TINY_NAME"
		domains="$ACME_TINY_DOMAINS"
		self_signed="$ACME_TINY_SELF_SIGNED"
	fi
}

parse_args() {
	while [ $# -gt 0 ]; do
		arg="$1"
		case $arg in
			-b)
                                shift
			        numbits="$1"
				;;
			-r)
			        shift
                                account_email="$1"
				if [ -z "$account_email" ]; then
				        usage
				fi
				;;
			-n)
			        shift
                                name="$1"
				if [ -z "$name" ]; then
				        usage
				fi
				;;
			-c)
				shift
				config_path="$1"
				if [ ! -f "$config_path" ] && [ ! -d "$config_path" ]; then
					usage
				fi
				;;
			-s)
				self_signed=1
				;;
			*)
			        domains="$domains $1"
				;;
		esac
		shift
	done
}

create_account_key() {
	if [ ! -f "$1" ]; then
		_umask="$(umask)"
		umask 0277
		/usr/bin/openssl genrsa \
			-out "$1" \
			"$numbits"
		umask "$_umask"
	fi
}

create_san() {
	_domains="$1"
	unset _san

	for _domain in $_domains; do
		_san="${_san}${_san:+,}DNS:${_domain}"
	done
	echo "$_san"
}

create_key() {
	_name="$1"

	if [ ! -f "${OPENSSL_KEY_DIR}/${_name}.key" ]; then
		_umask="$(umask)"
		umask 0077
		/usr/bin/openssl genrsa \
			-out "${OPENSSL_KEY_DIR}/${_name}.key" \
			"$numbits"
		umask "$_umask"
		/bin/rm -f "${OPENSSL_CERT_DIR}/${_name}.csr"
	fi
}

create_csr() {
	_name="$1"
	shift
	_domain="$1"
	_domains="$*"

	if [ ! -f "${OPENSSL_CERT_DIR}/${_name}.csr" ]; then
		if [ "$_domains" != "$_domain" ]; then
			_san="$(create_san "$_domains")"

			/usr/bin/openssl req \
				-new \
				-sha256 \
				-key "${OPENSSL_KEY_DIR}/${_name}.key" \
				-subj "/" \
				-addext "subjectAltName = $_san" \
				-out "${OPENSSL_CERT_DIR}/${_name}.csr"
		else
			/usr/bin/openssl req \
				-new \
				-sha256 \
				-key "${OPENSSL_KEY_DIR}/${_name}.key" \
				-subj "/CN=${_domain}" \
				-out "${OPENSSL_CERT_DIR}/${_name}.csr"
		fi
	fi
}

check_crt() {
	_name="$1"

	if [ ! -f "${OPENSSL_CERT_DIR}/${_name}.crt" ]; then
		return 1
	fi
	printf "%s: " "$_name"
	if /usr/bin/openssl x509 \
			    -checkend "$((DAYS * 86400))" \
			    -in "${OPENSSL_CERT_DIR}/${_name}.crt"; then
		return 0
	fi

	return 1
}

get_crt() {
	_name="$1"
	_account_email="$2"

	_temp_crt="$(mktemp)"
	/usr/local/bin/acme-tiny \
		--contact "mailto:$_account_email" \
		--account-key "$ACCOUNT_KEY_FILE" \
		--csr "${OPENSSL_CERT_DIR}/${_name}.csr" \
		--acme-dir "$CHALLENGES_DIR" > "$_temp_crt"
	/bin/cat "$_temp_crt" > "${OPENSSL_CERT_DIR}/${_name}.crt"
	/bin/rm "$_temp_crt"
	touch "${OPENSSL_CERT_DIR}/${_name}.stamp"
}

validate_vars() {
		if [ -z "$domains" ] || [ -z "$name" ]; then
			usage
		fi
		if [ -z "$account_email" ] && [ -z "$self_signed" ]; then
			usage
		fi
}

create_crt() {
	_name="$1"
	_account_email="$2"
	_domains="$3"

	/bin/mkdir -p "$OPENSSL_KEY_DIR" "$OPENSSL_CERT_DIR" "$CHALLENGES_DIR"
	create_account_key "$ACCOUNT_KEY_FILE"
	create_key "$_name"
	# shellcheck disable=SC2086
	create_csr "$_name" $_domains
	if ! check_crt "$_name"; then
		get_crt "$_name" "$_account_email"
	fi
}

create_self_signed() {
	_name="$1"
	shift
	_domain="$1"
	_domains="$*"

	if ! check_crt "$_name"; then
		if [ "$_domains" != "$_domain" ]; then
			_san="$(create_san "$_domains")"

			/usr/bin/openssl req \
					 -x509 \
					 -newkey \
					 rsa:"$numbits" \
					 -sha256 \
					 -days 3650 \
					 -nodes \
					 -keyout "${OPENSSL_KEY_DIR}/${_name}.key" \
					 -subj "/" \
					 -addext "subjectAltName = $_san" \
					 -out "${OPENSSL_CERT_DIR}/${_name}.crt"
		else
			/usr/bin/openssl req \
					 -x509 \
					 -newkey \
					 rsa:"$numbits" \
					 -sha256 \
					 -days 3650 \
					 -nodes \
					 -keyout "${OPENSSL_KEY_DIR}/${_name}.key" \
					 -subj "/CN=${_domain}" \
					 -out "${OPENSSL_CERT_DIR}/${_name}.crt"
		fi
		touch "${OPENSSL_CERT_DIR}/${_name}.stamp"
	fi
}

numbits="4096"
parse_args "$@"

if [ -n "$config_path" ]; then
	if [ -d "$config_path" ]; then
		_config_files="$config_path/*.conf"
	else
		_config_files="$config_path"
	fi

	for _config_file in $_config_files; do
		read_config "$_config_file"
		validate_vars
	done

	for _config_file in $_config_files; do
		read_config "$_config_file"
		if [ -z "$self_signed" ]; then
			create_crt "$name" "$account_email" "$domains"
		else
			create_self_signed "$name" "$domains"
		fi
	done
else
	validate_vars
	if [ -z "$self_signed" ]; then
		create_crt "$name" "$account_email" "$domains"
	else
		create_self_signed "$name" "$domains"
	fi
fi
